// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DrivetrainConstants;

public class Drivetrain extends SubsystemBase {

  private TalonSRX leftFront;
  private TalonSRX leftBack;
  private TalonSRX rightFront;
  private TalonSRX rightBack;

  /** Creates a new Drivetrain. */
  public Drivetrain() {
    leftFront = new TalonSRX(DrivetrainConstants.LEFT_FRONT);
    leftBack = new TalonSRX(DrivetrainConstants.LEFT_BACK);
    rightFront = new TalonSRX(DrivetrainConstants.RIGHT_FRONT);
    rightBack = new TalonSRX(DrivetrainConstants.RIGHT_BACK);

    leftFront.configFactoryDefault();
    leftBack.configFactoryDefault();
    rightFront.configFactoryDefault();
    rightBack.configFactoryDefault();

    rightBack.setInverted(true);
    rightFront.setInverted(true);

    rightBack.follow(rightFront);
    leftBack.follow(leftFront);
  }

  public void arcadeDrive(double forward, double turn) {
    leftFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, turn);
    rightFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
  }
}
